//
//  ElectronicsViewController.swift
//  CustomDelegate
//
//  Created by Sierra 4 on 31/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
class ElectronicsViewController: UIViewController {
    var delegate : MyCustomDelegate?
    @IBOutlet var upperEView: UIView!
    @IBOutlet var ElctTblOutlet: UITableView!
    var arrayItems =
        [TableClass (labelText:"Air Conditioning"),TableClass (labelText:"Appliance plug"),TableClass (labelText:"Attic fan"),TableClass (labelText:"Microwave oven"),TableClass (labelText:"Aroma lamp"),TableClass (labelText:"Refrigerator"),TableClass (labelText:"Window fan"),TableClass (labelText:"Radiator"),TableClass (labelText:"Washing machine"),TableClass (labelText:"Television")]
    override func viewDidLoad() {
        super.viewDidLoad()
        upperEView.layer.shadowOffset = CGSize.zero
        upperEView.layer.shadowOpacity = 5
        upperEView.layer.shadowRadius = 3
        upperEView.layer.shadowColor = UIColor.yellow.cgColor
    }
    @IBAction func backBtn(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
    extension ElectronicsViewController : UITableViewDelegate, UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrayItems.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:ElectronicsTableViewCell = tableView.dequeueReusableCell(withIdentifier:"ElectronicaIdentifier", for: indexPath) as!ElectronicsTableViewCell
            cell.object = arrayItems[indexPath.row]
            return cell
        }
//        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            let indexpath = tableView.indexPathForSelectedRow
//            let cell = tableView.cellForRow(at: indexPath) as! ElectronicsTableViewCell
//            let controller = parent as! ViewController
//            controller.showData(text : cell.ElectronicsLbl.text ?? "")
//        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            delegate?.didAddedSubView(items: arrayItems[indexPath.row].label1!)
        }
    }

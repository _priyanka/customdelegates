//
//  FurnitureViewController.swift
//  CustomDelegate
//
//  Created by Sierra 4 on 31/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class FurnitureViewController: UIViewController {
    var delegate : MyCustomDelegate?
    
    @IBOutlet var upperFView: UIView!
    @IBOutlet var furTblOutlet: UITableView!
    var arrayItems =
        [TableClass (labelText:"Chair"),TableClass (labelText:"Bean bag"),TableClass (labelText:"Stool"),TableClass (labelText:"Couch"),TableClass (labelText:"Bed"),TableClass (labelText:"Headboard"),TableClass (labelText:"Sofa bed"),TableClass (labelText:"Chess table"),TableClass (labelText:"Cupboard"),TableClass (labelText:"Dining set")
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        upperFView.layer.shadowColor = UIColor(red: 61/255, green: 178/255, blue: 209/255, alpha: 1.0).cgColor
        upperFView.layer.shadowRadius = 3
        upperFView.layer.shadowOffset = CGSize.zero
        upperFView.layer.shadowOpacity = 5
    }
    @IBAction func backBtn(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension FurnitureViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FurnitureTableViewCell = tableView.dequeueReusableCell(withIdentifier:"FurnitureIdentifier", for: indexPath) as!FurnitureTableViewCell
        cell.object = arrayItems[indexPath.row]
        return cell
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let indexpath = tableView.indexPathForSelectedRow
//        let cell = tableView.cellForRow(at: indexPath) as! FurnitureTableViewCell
//        let controller = parent as! ViewController
//        controller.showData(text : cell.FurnitureLbl.text ?? "")
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didAddedSubView(items: arrayItems[indexPath.row].label1!)
    }
}

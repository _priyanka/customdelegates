//
//  GroceryViewController.swift
//  CustomDelegate
//
//  Created by Sierra 4 on 31/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class GroceryViewController: UIViewController {
    var delegates : MyCustomDelegate?
    @IBOutlet var UpperGView: UIView!
    @IBOutlet var tableViewOutlet: UITableView!
    var arrayItems =
        [TableClass (labelText:"Beverages"),TableClass (labelText:"Snack Foods"),TableClass (labelText:"Cooking Supplies"),TableClass (labelText:"Desserts"),TableClass (labelText:"Squashes"),TableClass (labelText:"Dried Fruits, Nuts"),TableClass (labelText:"Spices & Masalas"),TableClass (labelText:"Rice, Flour & Pulses"),TableClass (labelText:"Chocolates"),TableClass (labelText:"Sweet Dishes")]

    override func viewDidLoad() {
        super.viewDidLoad()
        UpperGView.layer.shadowColor = UIColor.red.cgColor
        UpperGView.layer.shadowRadius = 3
        UpperGView.layer.shadowOpacity = 5
        UpperGView.layer.shadowOffset = CGSize.zero
    }

    @IBAction func backBtn(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension GroceryViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayItems.count
}
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:GroceryTableViewCell = tableView.dequeueReusableCell(withIdentifier:"GroceryIdentifier", for: indexPath) as!GroceryTableViewCell
        cell.object = arrayItems[indexPath.row]
        return cell
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let indexpath = tableView.indexPathForSelectedRow
//        let cell = tableView.cellForRow(at: indexPath) as! GroceryTableViewCell
//        let controller = parent as! ViewController
//        controller.showData(text : cell.GroceryLbl.text ?? "")
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegates?.didAddedSubView(items: arrayItems[indexPath.row].label1!)
    }
}

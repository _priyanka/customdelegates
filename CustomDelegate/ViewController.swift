//
//  ViewController.swift
//  CustomDelegate
//
//  Created by Sierra 4 on 31/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var mainLbl: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet var viewOne: UIView!
    @IBOutlet var viewTwo: UIView!
    @IBOutlet var viewThree: UIView!
    @IBOutlet var btnOne: UIButton!
    @IBOutlet var btnTwo: UIButton!
    @IBOutlet var btnThree: UIButton!
    @IBOutlet var lblOutlet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOne.layer.cornerRadius = 36
        viewTwo.layer.cornerRadius = 36
        viewThree.layer.cornerRadius = 36
        btnOne.layer.cornerRadius = 19
        btnTwo.layer.cornerRadius = 19
        btnThree.layer.cornerRadius = 19
        lblOutlet.layer.borderWidth = 0.5
        lblOutlet.layer.borderColor = UIColor.white.cgColor
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func FirstBtnAction(_ sender: UIButton) {
        let controller = storyboard!.instantiateViewController(withIdentifier: "GroceryStory") as! GroceryViewController
        controller.delegates = self
        addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            controller.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            controller.view.topAnchor.constraint(equalTo: view.topAnchor, constant: 105),
            controller.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80)
            ])
        
        controller.didMove(toParentViewController: self)
    }
    @IBAction func secondBtnAction(_ sender: UIButton) {
        let controller = storyboard!.instantiateViewController(withIdentifier: "ElectronicsStory") as! ElectronicsViewController
        controller.delegate = self
        addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            controller.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            controller.view.topAnchor.constraint(equalTo: view.topAnchor, constant: 105),
            controller.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80)
            ])
        
        controller.didMove(toParentViewController: self)
    }
    @IBAction func ThirdBtnAction(_ sender: UIButton) {
        let controller = storyboard!.instantiateViewController(withIdentifier: "FurnitureStory") as! FurnitureViewController
        controller.delegate = self
        addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            controller.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            controller.view.topAnchor.constraint(equalTo: view.topAnchor, constant: 105),
            controller.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80)
            ])
        
        controller.didMove(toParentViewController: self)
    }
}
//    func showData(text: String) {
//                mainLbl.text = text
//        }
    extension ViewController:MyCustomDelegate{
        func didAddedSubView(items: String) {
            self.mainLbl.text = items
        }
}



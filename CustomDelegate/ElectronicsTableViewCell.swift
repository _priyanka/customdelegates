//
//  ElectronicsTableViewCell.swift
//  CustomDelegate
//
//  Created by Sierra 4 on 31/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ElectronicsTableViewCell: UITableViewCell {

    @IBOutlet var ElectronicsLbl: UILabel!
    var object: TableClass?{
        didSet{
            updateUI()
        }
    }
    fileprivate func updateUI(){
        ElectronicsLbl?.text = object?.label1
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
